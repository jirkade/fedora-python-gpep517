Name:           python-gpep517
Version:        15
Release:        %autorelease
Summary:        Python package builder and installer for non-pip-centric world

# Check if the automatically generated License and its spelling is correct for Fedora
# https://docs.fedoraproject.org/en-US/packaging-guidelines/LicensingGuidelines/
License:        MIT
URL:            https://pypi.org/project/gpep517/
Source:         %{pypi_source gpep517}

BuildArch:      noarch
BuildRequires:  python3-devel


# Fill in the actual package description to submit package to Fedora
%global _description %{expand:
This is package 'gpep517' generated automatically by pyp2spec.}


%description %_description

%package -n     python3-gpep517
Summary:        %{summary}

%description -n python3-gpep517 %_description


%prep
%autosetup -p1 -n gpep517-%{version}


%generate_buildrequires
%pyproject_buildrequires


%build
%pyproject_wheel


%install
%pyproject_install
# For official Fedora packages, including files with '*' +auto is not allowed
# Replace it with a list of relevant Python modules/globs and list extra files in %%files
%pyproject_save_files '*' +auto


%check
%pyproject_check_import -t


%files -n python3-gpep517 -f %{pyproject_files}


%changelog
%autochangelog
